package domain;

public class BankApplication {

private float loan;
private int instalmentAmount;
private float bankRate;
private float fixedPrice;
private String type;
private boolean goodData =true ;

public float getLoan() {
return loan;
}
public void setLoan(float loan) {
this.loan = loan;
}

public int getInstalmentAmount() {
return instalmentAmount;
}
public void setInstalmentAmount(int instalmentAmount) {
this.instalmentAmount = instalmentAmount;
}

public float getBankRate() {
return bankRate;
}
public void setBankRate(float bankRate) {
this.bankRate = bankRate;
}

public float getFixedPrice() {
return fixedPrice;
}
public void setFixedPrice( float fixedPrice) {
this.fixedPrice = fixedPrice;
}

public String getType() {
return type;

}

public void setType(String type) {
this.type = type;

}
public boolean getGoodData() {
return goodData;
}
public void setGoodData(boolean goodData) {
this.goodData = goodData;
}


}